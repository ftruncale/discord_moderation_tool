import { TokenService } from './storage.service'
import axios from 'axios'

const BASE_URL = process.env.VUE_APP_DISCORD_API_ENDPOINT

const OauthService = {

    // Stores the 401 interceptor position so that it can be later ejected when needed
    _401interceptor: null,

    getUser() {
        let auth_str = `Bearer ${TokenService.getToken()}`
        let resource = BASE_URL + "/users/@me"
        return axios.get(resource, {headers: { Authorization: auth_str}})
    },

    getGuilds() {
        let auth_str = `Bearer ${TokenService.getToken()}`
        let resource = BASE_URL + "/users/@me/guilds"
        return axios.get(resource, {headers: { Authorization: auth_str}})
    },

    /**
     * Perform a custom axios request.
     *
     * data is an object containing the following properties:
     *  - method
     *  - url
     *  - data ... request payload
     *  - auth (optional)
     *    - username
     *    - password
    **/
    customRequest(data) {
        data.url = BASE_URL + data.url
        return axios(data)
    },

    mount401Interceptor() {
       this._401interceptor = axios.interceptors.response.use(
           (response) => {
               return response
           },
           async (error) => {
               if (error.request.status == 401) {
                   if (error.config.url.includes('/oauth2/token')) {
                       // Refresh token has failed. Logout the user
                       this.store.dispatch('auth/logout')
                       throw error
                   } else {
                       // Refresh the access token
                       try{
                           await this.store.dispatch('auth/refreshToken')
                           // Retry the original request
                           return this.customRequest({
                               method: error.config.method,
                               url: error.config.url,
                               data: error.config.data
                           })
                       } catch (e) {
                           // Refresh has failed - reject the original request
                           throw error
                       }
                   }
               }

               // If error was not 401 just reject as is
               throw error
           }
       )
   },

   unmount401Interceptor() {
       // Eject the interceptor
       axios.interceptors.response.eject(this._401interceptor)
   }
}

export default OauthService
