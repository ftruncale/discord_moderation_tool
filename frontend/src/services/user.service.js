//Adapted from
//https://medium.com/@zitko/structuring-a-vue-project-authentication-87032e5bfe16

import OauthService from './oauth.service'
import { TokenService } from './storage.service'


class AuthenticationError extends Error {
    constructor(errorCode, message) {
        super(message)
        this.name = this.constructor.name
        this.message = message
        this.errorCode = errorCode
    }
}

const UserService = {
    /**
     * Login the user and store the access token to TokenService.
     *
     * @returns access_token
     * @throws AuthenticationError
    **/
    login: async function(code) {
        const queryString = require('query-string');
        const requestData = {
            method: 'post',
            url: "/oauth2/token",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            data: {
                client_id:process.env.VUE_APP_DISCORD_CLIENT_ID,
                client_secret:process.env.VUE_APP_DISCORD_CLIENT_SECRET,
                grant_type:"authorization_code",
                code:code,
                scope:"identify guilds",
                redirect_uri:process.env.VUE_APP_DISCORD_REDIRECT_URI,
            },
        }

        try {
            requestData.data = queryString.stringify(requestData.data)
            const response = await OauthService.customRequest(requestData)
            TokenService.saveToken(response.data.access_token)
            TokenService.saveRefreshToken(response.data.refresh_token)
            OauthService.setHeader()

            OauthService.mount401Interceptor();

            return response.data.access_token
        } catch (error) {
            console.log(error)
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },

    /**
     * Refresh the access token.
    **/
    refreshToken: async function() {
        const queryString = require('query-string');
        const refreshToken = TokenService.getRefreshToken()

        const requestData = {
            method: 'post',
            url: "/oauth2/token",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            data: {
                client_id :process.env.VUE_APP_DISCORD_CLIENT_ID,
                client_secret:process.env.VUE_APP_DISCORD_CLIENT_SECRET,
                grant_type: 'refresh_token',
                refresh_token: refreshToken
            },
        }
        try {
            requestData.data = queryString.stringify(requestData.data)
            const response = await OauthService.customRequest(requestData)
            TokenService.saveToken(response.data.access_token)
            TokenService.saveRefreshToken(response.data.refresh_token)
            // Update the header in ApiService
            OauthService.setHeader()

            return response.data.access_token
        } catch (error) {
            console.log(error)
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }

    },

    /**
     * Logout the current user by removing the token from storage.
     *
     * Will also remove `Authorization Bearer <token>` header from future requests.
    **/
    logout() {
        // Remove the token and remove Authorization header from Api Service as well
        TokenService.removeToken()
        TokenService.removeRefreshToken()
        OauthService.removeHeader()

        // NOTE: Again, we'll cover the 401 Interceptor a bit later.
        OauthService.unmount401Interceptor()
    }
}

export default UserService

export { UserService, AuthenticationError }
