import axios from 'axios'
const BASE_URL = process.env.VUE_APP_BACKEND_API_URL

const ApiService = {

    get(resource) {
        resource = BASE_URL + resource
        return axios.get(resource)
    },

    post(resource, data) {
        resource = BASE_URL + resource
        return axios.post(resource, data)
    },

    put(resource, data) {
        resource = BASE_URL + resource
        return axios.put(resource, data)
    },

    delete(resource) {
        resource = BASE_URL + resource
        return axios.delete(resource)
    },

    /**
     * Perform a custom axios request.
     *
     * data is an object containing the following properties:
     *  - method
     *  - url
     *  - data ... request payload
     *  - auth (optional)
     *    - username
     *    - password
    **/
    customRequest(data) {
        data.url = BASE_URL + data.url
        return axios(data)
    }
}

export default ApiService
