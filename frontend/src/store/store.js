import Vue from "vue";
import Vuex from "vuex";

import { auth } from './modules/auth'
import { cache } from './modules/cache'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    cache
  }
});
