import Vue from "vue";

const state =  {
    user_map: {},
}

const getters = {
  getUsername: (state) => (id) => {
    return state.user_map[id].username
  },
  getDiscriminator: (state) => (id) => {
    return state.user_map[id].disc
  },
  getAvatar: (state) => (id) => {
    return state.user_map[id].avatar
  },
  isInUserMap: (state) => (id) =>{
    return (id in state.user_map)
  }
}

const mutations = {
  addUser: function(state, payload){
    Vue.set(state.user_map, payload.user_id, payload)
  }
}

export const cache = {
    namespaced: true,
    state,
    getters,
    mutations
}
