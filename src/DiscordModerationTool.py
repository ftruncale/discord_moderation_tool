#Francesca 'Jade' Truncale
## @package DiscordModerationTool
# Bot & API Server tool for use with the frontend of this project.
#
# Logs messages from all guilds (servers) to a database.
#
# Creates an API server that can act as backend for Oauth2 with Discord.
#
##
import DGCdbConnector as DB_OBJ #used to hold database engine information
import DiscordHTTPClient as DHC
import DiscordGatewayClient as DGC

import asyncio
import signal
import logging
import sys
import configparser

from aiohttp import web
from aiohttp_oauth2 import oauth2_app
import aiohttp_cors


class DiscordModerationTool():
    ##
    # Starts the Tool immediately on creation.
    #
    # \param token User or Bot token, obtained from [Discord's Developer Portal](https://discord.com/developers/applications)
    # \param Name for the bot to self-identify.
    # \param type Database type. Valid entries are ``mysql, sqlite, postgresql``
    # \param host Database hostname.
    # \param user Database Username.
    # \param passowrd Database password.
    # \param loop Asyncio event loop to use if specified.
    # \param ws Enable the API server. Default is ``True``.
    # \param ws_hostname Hostname for the API Server
    # \param ws_port Port for the API server
    # \param ws_oauth_url Where to redirect the Oauth2 code after capture.
    # \param discord_id User or Bot discord id.
    # \param discord_secret Bot discord secret.
    # \param config Takes a string ``"dmt.ini"``, if specified, will create a config file or load from it.
    ##
    def __init__(self, token=None, name='DMTBot', \
                 type = 'sqlite', host = None, user = None, password = None, \
                 loop = None, \
                 ws = True,
                 ws_hostname = 'localhost',
                 ws_port = 3000,
                 ws_oauth_url = 'http://localhost:8080',
                 discord_id = None,
                 discord_secret = None,
                 config = None):

        #Set up logging format
        self._log = logging.getLogger("{name}".format(name=__name__))
        #Set up logger
        self._setupLogger()

        #If an event_loop is not supplied, make our own
        if (loop is None): #check if event loop is supplied for async
            self._event_loop = asyncio.new_event_loop()
        else:
            self._event_loop = loop

        asyncio.set_event_loop(self._event_loop)

        #If we have a config file, pull args from it
        if (config is not None):
            file = self._configure(config)
            if (file == 0):
                self._log.info("Config file created. Please add your relevant values.")
                self._log.info("Exiting...")
                return

        #If no config file, set vars according to arguments
        else:
            self._name = name
            self._token = token
            self._discordId = discord_id
            self._discordSecret = discord_secret

            self._ws = False
            self._wsActual = None
            self._wsRunner = None
            self._oauthCallbackUrl = ws_oauth_url

            #If webserver is enabled, initalize
            if (ws):
                self._ws = ws
                self._wsHostname = ws_hostname
                self._wsPort = ws_port

            self._dbType = type
            self._dbHost = host
            self._dbUser = user
            self._dbPassword = password

        if (self._token is None):
            self._log.error("Token is missing, {name} is exiting...")
            return

        if (self._ws):
            self._wsInit()

        #Initialize Modules
        self._gatewayApi = DGC.DiscordGateWayClient(token=self._token, name=self._name, loop = self._event_loop)
        self._httpApi = DHC.DiscordHTTPClient(token=self._token) #needed for http api calls, for banning/kicking and getting history
        self.db = DB_OBJ.DGCdbConnector(self._dbType, self._dbHost, self._dbUser, self._dbPassword) #db connector

        self._init()



    def _configure(self, config_file):
        config = configparser.ConfigParser()
        dataset = config.read(config_file)

        if (not len(dataset)): #if config file is empty
            config['Bot Authorization'] = {'token': '',
                                           'client_name': 'DMTBot'}

            config['Database'] = {'database_type': 'sqlite',
                                           'host':'',
                                           'user':'',
                                           'password':''}

            config['API Server'] = {'enabled': 'True',
                                    'hostname':'localhost',
                                    'port':'3000',
                                    'callback_url':'http://localhost:8080',
                                    'discord_id':'',
                                    'discord_secret':''}

            with open(config_file, 'w') as configfile:
                config.write(configfile)
            return 0

        else:
            self._token = config.get('Bot Authorization', 'token')
            self._name = config.get('Bot Authorization', 'client_name', fallback = 'DMTBot')
            self._dbType = config.get('Database', 'database_type', fallback='sqlite')
            self._dbHost = config.get('Database', 'host', fallback=None)
            self._dbUser = config.get('Database', 'user', fallback=None)
            self._dbPassword = config.get('Database', 'password', fallback=None)
            self._ws = config.getboolean('API Server','enabled', fallback=False)
            self._wsHostname = config.get('API Server', 'hostname', fallback='localhost')
            self._wsPort = config.get('API Server', 'port', fallback=3000)
            self._oauthCallbackUrl = config.get('API Server', 'callback_url', fallback='http://localhost:8080')
            self._discordId = config.get('API Server', 'discord_id')
            self._discordSecret = config.get('API Server', 'discord_secret')


    def _setupLogger(self):
        self._log.setLevel(logging.DEBUG)
        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.DEBUG)
        formatter=logging.Formatter('[%(levelname)s] %(asctime)s - %(message)s')
        handler.setFormatter(formatter)
        self._log.addHandler(handler)

    async def _getPayloads(self): #gets payloads from DGC's list to process
        while True:
            try:
                await asyncio.sleep(0.1)
                if (self._gatewayApi.hasPayload()):
                    await self._processPayload(await self._gatewayApi.getPayload())#pop first payload off list and process
            except asyncio.CancelledError:
                raise


    async def _processPayload(self, payload): #process deserialized payload
        type = payload.get('t') #get the type of payload, then process
        data = payload.get('d') #get the actual data of the message
        if (type == 'MESSAGE_CREATE'):
            self.db.addMessage(data)
            self.db.commit()

        elif (type == 'MESSAGE_UPDATE'):
            full_message = await self._httpApi.getMessage(message_id = data.get('id'), channel_id = data.get('channel_id'))
            self.db.updateMessage(full_message)
            self.db.commit()

        elif (type == 'MESSAGE_DELETE'):
            self.db.markMessageAsDeleted(data)

        elif (type == 'GUILD_CREATE'):
            await self._updateMessageLog(data)
            await self._updateChannels(data)


    async def _updateChannels(self, payload): #adds channels to db from guild create event
        guild_id = payload.get('id')
        for channel in payload.get('channels'):
            if (not self.db.channelExists(channel.get('id'))): #if channel does not exist
                self.db.addChannel(channel, guild_id)
                self.db.commit()
                return


    async def _updateMessageLog(self, payload): #gets all previous messages and logs them to db for all channels available
        for channel in payload.get('channels'):
            if (channel.get('type') == 0): #regular text channel type
                messages = []
                last_message_id_saved = self.db.getLastMessageId(channel.get('id'))
                if (channel.get('last_message_id') is None): #channel must have no messages, ignore
                    pass

                elif (last_message_id_saved is None): #we've backed up zero messages so far
                    messages = await self._httpApi.getMessages(channel.get('id'), limit=100)
                    while (len(messages) > 0):
                        for message in messages:
                            self.db.addMessage(message)

                        self.db.commit()
                        last_message = messages[-1].get('id')#get last message id, loop
                        messages = await self._httpApi.getMessages(channel.get('id'), message_id=last_message, when='before', limit=100)

                elif (str(last_message_id_saved) < channel.get('last_message_id')): #if last saved id is older than the channel one
                    messages = await self._httpApi.getMessages(channel_id = channel.get('id'), message_id=last_message_id_saved, when='after', limit=100)
                    while (len(messages) > 0):
                        for message in messages:
                            self.db.addMessage(message)

                        self.db.commit()
                        last_message = messages[0].get('id')#get first message id, loop
                        messages = await self._httpApi.getMessages(channel.get('id'), when='after', message_id=last_message, limit=100)

    ##
    # Shuts down all components of the tool.
    ##
    async def shutdown(self):
        self._log.info("Shutting Down {name}".format(name=self._name))
        tasks = [t for t in asyncio.all_tasks() if t is not
                 asyncio.current_task()]
        [task.cancel() for task in tasks]
        await asyncio.gather(*tasks, return_exceptions=True)
        await self._gatewayApi.stop()

        if (self._ws):
            await self._wsRunner.cleanup()

        self._event_loop.stop()

        self._log.info("{name} successfully shutdown.".format(name=__name__))

    def _setupSignalHandlers(self):
        signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
        for s in signals:
            self._event_loop.add_signal_handler(s, lambda  s=s: self._event_loop.create_task(self.shutdown()))

    def _init(self):
        self._setupSignalHandlers()

        try:
            self._log.info("{client_name} ({app_name}) is starting...".format(client_name=__name__, app_name=self._name))
            self._event_loop.create_task(self._gatewayApi.start())
            self._event_loop.create_task(self._getPayloads())
            if (self._ws):
                self._log.info("API Server ({name}) now starting on {host}:{port}...".format(name=self._name,
                                                                                             host=self._wsHostname,
                                                                                             port=self._wsPort))
                self._event_loop.create_task(self._wsActual.start())
            self._event_loop.run_forever()
        #https://www.roguelynn.com/words/asyncio-graceful-shutdowns/
        #Graceful exit
        finally:
            pass
            # self._event_loop.stop()


    #BEGIN: API Setup
    def _wsInit(self):
        app = web.Application()

        # setup routes
        app.router.add_post('/messages', self._getMessages)
        app.router.add_post('/user', self._getUser)
        app.router.add_post('/channels', self._getChannels)
        # app.router.add_get('/discord/callback', self._routeLoginCode)

        #to allow cors to make post requests from an internal server
        cors = aiohttp_cors.setup(app, defaults={
                "*": aiohttp_cors.ResourceOptions(
                    allow_credentials=True,
                    expose_headers=("X-Custom-Server-Header",),
                    allow_headers=("X-Requested-With", "Content-Type"),
                    max_age=3600
            )
        })


        # create oauth endpoint
        app.add_subapp(
            "/discord/",  # any arbitrary prefix
            oauth2_app(
                client_id=self._discordId,
                client_secret=self._discordSecret,
                authorize_url="https://discord.com/api/oauth2/authorize",
                token_url="https://discord.com/api/oauth2/token",
                # add scopes if you want to customize them
                scopes=['identify', 'guilds'],
                # optionally add an on_login coroutine to handle the post-login logic
                # it should expect the request and the oauth2 access code response
                on_login=self._routeLoginCode,
                # on_error=self._routeLoginCode,
            ),
        )

        for route in list(app.router.routes()):
            if not (route.method == '*'): #workaround!
                cors.add(route)

        self._wsRunner = web.AppRunner(app)
        self._event_loop.run_until_complete(self._wsRunner.setup())
        self._wsActual = web.TCPSite(self._wsRunner, self._wsHostname, self._wsPort)

    async def _routeLoginCode(self, request):
        code = request.rel_url.query['code']
        return web.HTTPFound("{url}/auth?code=".format(url=self._oauthCallbackUrl)+code)
        # return web.json_response(response, status=200)

    async def _getMessages(self, request):
        request_body = await request.post()
        message_id = request_body.get('message_id')
        channel_id = request_body.get('channel_id')
        limit = int(request_body.get('limit'))

        response = self.db.getMessages(channel_id=channel_id, message_id=message_id, limit=limit)
        return web.json_response(response, status=200)

    async def _getChannels(self, request):
        request_body = await request.post()
        guild_id = request_body.get('id')
        response = self._gatewayApi.guilds.get(guild_id)
        if (response is not None):
            response = response.get('channels')
            return web.json_response(response, status=200)

        else:
            return web.Response(status=404)
        # response = self.db.getChannels(guild_id=guild_id)


    async def _getUser(self, request):
        request_body = await request.post()
        user_id = request_body.get('id')
        # print(user_id)
        user = await self._httpApi.getUser(user_id)
        response = {'username' : user.get('username', 'Anonymous'), \
                    'discriminator' : user.get('discriminator', '0000'), \
                    'avatar' : user.get('avatar'), \
                    'user_id' : user_id}
        return web.json_response(response, status=200)
