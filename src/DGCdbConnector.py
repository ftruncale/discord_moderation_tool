#Francesca 'Jade' Truncale, 2020
## @package DiscordGatewayClientDatabaseConnector(DGCdbConnector)
# Allows access to a database of choice for the DGC Client.
# Supports MariaDB/MySQL, PostgreSQL, SQLite
##
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import desc
from sqlalchemy_utils import database_exists, create_database #to assist with database functions
from sqlalchemy.orm.exc import MultipleResultsFound

from models import *

import json

#helper class for the DiscordGateWayClient, manages the database connection and schema
#if made with no arguments, creates/connects to a sqlite database for use
#host should be in format host:port/db
class DGCdbConnector:
    def __init__(self, type = 'sqlite', host = '', user = None, password = None):
        self._uri = None #used to create the uri for the engine
        self._engine = None #stores the engine

        self._type = type

        self._session = None #stores db session

        self._init(type, host, user, password)


    #https://leportella.com/sqlalchemy-tutorial.html
    def _init(self, type, host, user, password):
        if (type == 'sqlite'): #sqlite does not need most other fields, reformat
            if (host == ''): #if a db is not specified, create one
                host = 'default.db'
                self._uri = "sqlite:///{path}".format(path=host)

        else: #format for most db types
            adjusted_type = ''
            if (type == 'mysql' or type == 'mariadb'): #use pymysql driver for mysql/mariadb
                adjusted_type = 'mysql+pymysql'

            adjusted_type = type

            self._uri = "{driver}://{u}:{p}@{h}".format(
                driver = adjusted_type, u=user, p=password, h=host) #using the pymysql driver

        #if database does not exist, create
        if (not database_exists(self._uri)):
            create_database(self._uri, encoding='utf-8')

        self._engine = create_engine(self._uri, encoding='utf-8')
        Base.metadata.create_all(self._engine) #create tables if they don't exist

        Session = sessionmaker(bind = self._engine)
        self._session = Session() #create session before functions
        self._createTriggers()

    def _createTriggers(self):
        pass
        # trigger_to_add = '' #will contain the trigger to execute
        # drop_command = '' #will clear trigger to update
        #
        # mysql_trigger = """
        # CREATE TRIGGER add_channel
        #     AFTER INSERT
        #     ON message FOR EACH ROW
        # BEGIN
        #     IF NOT EXISTS (SELECT id FROM channel where channel.id = new.channel_id) THEN
        #         INSERT INTO channel(id, guild_id)
        #         VALUES(new.channel_id, new.guild_id);
        #     END IF;
        # END
        # """
        #
        # mysql_drop = """DROP TRIGGER IF EXISTS add_channel ON DATABASE;"""
        #
        # sqlite_trigger = """
        # CREATE TRIGGER add_channel
        #     AFTER INSERT
        #     ON message FOR EACH ROW
        #     WHEN new.channel_id not in (select id from channel)
        # BEGIN
        #     INSERT INTO channel(id, guild_id)
        #     VALUES(new.channel_id, new.guild_id);
        # END
        # """
        #
        # sqlite_drop = """DROP TRIGGER IF EXISTS add_channel"""
        #
        # if (self._type == 'sqlite'):
        #     trigger_to_add = sqlite_trigger
        #     drop_command = sqlite_drop
        #
        # elif (self._type == 'mysql' or self._type == 'mariadb'):
        #     trigger_to_add = mysql_trigger
        #     drop_command = mysql_drop
        #
        # self._session.execute(drop_command)
        # self._session.execute(trigger_to_add)

    #commit  changes to db
    def commit(self):
        self._session.commit()
        # self._refreshSession()

    #Expects payload, type expected of gateway payload object
    def addMessage(self, payload):
        embed = payload.get('embeds')
        attachment = payload.get('attachments')

        if (embed):
            for item in embed:
                embed_cmd = Embed(message_id = payload.get('id'), type = item.get('type'))
                self._session.add(embed_cmd)
            embed = True
        else:
            embed = False

        if (attachment):
            for item in attachment:
                attachment_cmd = Attachment(id = item.get('id'), message_id=payload.get('id'), filename = item.get('filename'), url=item.get('url'))
                self._session.add(attachment_cmd)
            has_attachment = True
        else:
            has_attachment = False

        message = Message(id = payload.get('id'),
                          user_id = payload.get('author').get('id'),
                          channel_id = payload.get('channel_id'),
                          timestamp = payload.get('timestamp'),
                          edited_timestamp = '', #on message create, no info
                          has_attachment = has_attachment,
                          content = payload.get('content'),
                          has_embed = embed,
                          deleted = False
                          )

        self._session.add(message)

    def addChannel(self, payload, guild_id):
        channel = Channel(id = payload.get('id'),
                          guild_id = guild_id
                         )

        self._session.add(channel)

    def markMessageAsDeleted(self, message):
        self._session.query(Message).filter(Message.id == message.get('id')).\
                                     update({'deleted' : True})


    def updateMessage(self, message):
        self._session.query(Message).filter(Message.id == message.get('id')).\
                                     update({'edited_timestamp' : message.get('edited_timestamp')})

        edited_message = Edited_Message(message_id = message.get('id'),
                                        edited_timestamp = message.get('edited_timestamp'),
                                        content = message.get('content'))

        self._session.add(edited_message)

    #returns a boolean if channel is found in channel table
    def channelExists(self, channel_id):
        result = self._session.query(Channel.id).\
                               filter(Channel.id == channel_id).first()

        if (result == None):
            return False

        return True

    def getLastMessageId(self, channel_id):
        result = self._session.query(Message.id).\
                               filter(Message.channel_id == channel_id).\
                               order_by(desc(Message.timestamp)).first()

        if (result != None):
            result = result[0]

        return result


    def getMessages(self, channel_id, message_id = None, limit = 5):
        if (limit > 100): #don't allow pulling more than 100  messages at a time
            limit = 100

        query = self._session.query(Message).\
                               filter(Message.channel_id == channel_id)

        if (message_id is not None):
            query = query.filter(Message.id < message_id)

        query = query.order_by(desc(Message.timestamp))

        result = query.limit(limit).all()

        message_schema = MessageSchema()
        serialized = message_schema.dump(result, many=True)
        return serialized

    # def getChannels(self, guild_id):
    #     query = self._session.query(Channel).\
    #                            filter(Channel.guild_id == id)
    #
    #
    #     result = query.all()
    #
    #     channel_schema = ChannelSchema()
    #     serialized = channel_schema.dump(result, many=True)
    #     return serialized
