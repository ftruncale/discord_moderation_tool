#Francesca 'Jade' Truncale, 2020

## @package DiscordGatewayClient
#  Wrapper for Discord's Gateway API, located here:
#
# https://discordapp.com/developers/docs/topics/gateway
##

import requests #to make get and post requests
import websockets #to  to the websocket
import asyncio #so we can do other things while ed
import ssl #discord uses wss (ws over tls)
import json #choosing to deal with json replies from the websocket

import platform #used to identify os properly in identify packet
import logging
import sys

from collections import deque

##
# # Gateway Payload Object
# \brief A helper class, of the format shown in the [Discord Documentation](https://discordapp.com/developers/docs/topics/gateway#sending-payloads-example-gateway-dispatch)
##
class GatewayPayloadObject:
    def __init__(self, op, d={}, s=None, t=None):
        ##Opcode for the payload
        self.op = op
        ##Event data/Payload data
        self.d = d
        ##Sequence number, used for resuming sessions and heartbeats
        self.s = s
        ##The event name for the payload.
        self.t = t

    def json(self):
        returnval = {"op":self.op, "d":self.d}

        #opcode 0: Dispatch, requires s and t fields
        if (self.op == 0):
            returnval.update({"s":self.s, "t":self.t})

        return json.dumps(returnval)

##
#  Gateway client
#
#  Wrapper for Discord's Gateway API, located here:
#
# https://discordapp.com/developers/docs/topics/gateway
##
class DiscordGateWayClient:
    ##
    # \param token User or Bot token, obtained from [Discord's Developer Portal](https://discord.com/developers/applications)
    # \param name The name the client will internally identify as (in logs).
    # \param loop An asyncio event loop, will be used internally if specified.
    def __init__(self, token, name, loop = None):
        ##Current Discord API version, change only if necessary.
        self.api_version =  '6';
        ##Encoding option, default is ``"json"``, change only if necessary.
        self.encoding = 'json'
        ##Base URL for Discord endpoint, change only if endpoint has changed. Default is ``'https://discordapp.com/api/'``
        self.base_url = 'https://discordapp.com/api/'

        self._cached_gateway = None #recommended to cache gateway address by api documentation
        self._websocket = None #websocket, assigned in self.connect

        self._session_id =  None #used to store session_id for use on resume
        self._user_obj = None #may store user object in future
        ##Private Channels (Direct Messages) accessible to the user.
        self.private_channels = [] #locally cache private channels
        ##Guilds (Servers) accessible to the user.
        self.guilds = {} #locally cache guilds

        self._heartbeat_ACK = 1 #make sure heartbeats are acknowledged
        self._sequence_num = {} #used in heartbeats and receieved in opcode 0, sequence num

        self._token = token #token for the bot
        self._name = name #name of the bot

        self._event_loop = loop

        if (loop is None):
            self._event_loop = asyncio.get_event_loop()

        self._payloads = asyncio.Queue(loop=self._event_loop) #list of payloads that need further processing (OPcode 0)
        self._log = logging.getLogger("{name}".format(name=__name__))



        self._init()

    def _init(self): #gets the current discord gateway
        #logging block
        self._log.setLevel(logging.DEBUG)
        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.DEBUG)
        formatter=logging.Formatter('[%(levelname)s] %(asctime)s - %(message)s')
        handler.setFormatter(formatter)
        self._log.addHandler(handler)

        response = requests.get(self.base_url + 'v{version}/gateway'.format(version=self.api_version))
        #current version of the api as of writing, gets the gateway for use in ing
        decoded_response = json.loads(response.text) #response is in json, decode to get values
        self._cached_gateway = decoded_response.get('url') #recommended to cache the  gateway address by the api documentation


    #BEGIN: Gateway API functions
    async def _connect(self, base_url, args):
        arg_string = "/?"
        for name, arg in args.items(): #build argument list for use in get-styled arguments
            arg_string += name + "=" + arg + "&"

        self._websocket = await websockets.connect(self._cached_gateway+arg_string, ssl = True); #secure connection
        result = await self._websocket.recv()
        return result

    #begin heartbeat based on provided interval from connect
    async def _heartbeat(self, heartbeat_interval):
        hb_payload = GatewayPayloadObject(op=1)#must send opcode 1 every heartbeat_interval
        while self._heartbeat_ACK:
            self._heartbeat_ACK = 0
            hb_payload.d = self._sequence_num #keep sequence_num up to date
            await self._websocket.send(hb_payload.json()) #must send opcode 1 every heartbeat_interval
            await asyncio.sleep(heartbeat_interval/1000) #interval is provided in ms, convert to s
        await self._reconnect(reason="Heartbeat ACK not receieved") #if we did not receive acknowledge, begin reconnect


    #receieves payloads when available
    async def _receivePayloads(self):
        while True:
            try:
                payload = await self._websocket.recv()
                self._event_loop.create_task(self._processPayload(payload))
            except asyncio.CancelledError:
                raise
                return
            except websockets.ConnectionClosed as e:
                self._log.error("Connection Closed from Discord API: {error}".format(error=e))
                await self.stop()
                return


    #Identify structure defined here:
    #https://discordapp.com/developers/docs/topics/gateway#identify
    async def _identify(self):
        payload = GatewayPayloadObject(op=2) #Opcode 2 is for Identify
        properties = {"$os":platform.system(), "$browser":self._name, "$device":self._name}
        identify_dict = {"properties":properties, "token":self._token}
        payload.d.update(identify_dict)
        await self._websocket.send(payload.json())

    async def _resume(self):
        payload = GatewayPayloadObject(op=6, d=self._sequence_num) #opcode 6 to resume
        d = {"token":self._token, "session_id":self._session_id, "seq":self._sequence_num}
        await self._websocket.send(payload.json())

    #opcodes defined as follow:
    #https://discordapp.com/developers/docs/topics/opcodes-and-status-codes
    async def _processPayload(self, payload):
        ds_payload = json.loads(payload) #deserialize payload into dict
        opcode = ds_payload.get('op')
        data = ds_payload.get('d')
        #process each opcode differently
        if (opcode == 0): #Dispatch, some event has occured
            self._sequence_num = ds_payload.get('s')
            type = ds_payload.get('t')
            if (type == 'READY'):
                await self._processReadyEvent(data)

            elif (type == "GUILD_CREATE"):
                # print(ds_payload)
                await self._payloads.put(ds_payload)
                await self._processGuildCreate(data)

            else:
                await self._payloads.put(ds_payload)

        elif (opcode == 1): #heartbeat receieved
            pass

        elif (opcode == 7): #reconnect requested by the gateway
            await self._reconnect(code=1000, reason="reconnect requested", resume = True)

        elif (opcode == 9): #invalid session
            #if can resume, do resume
            await self._reconnect(code=1001, reason="invalid session", resume=data)

        elif (opcode == 10):  #Hello from gateway, start heartbeat
            self._event_loop.create_task(self._heartbeat(data.get('heartbeat_interval')))

        elif (opcode == 11): #Heartbeat ACK(nowledged)
            self._heartbeat_ACK = 1

        else:
            return 0

    async def _processReadyEvent(self, data):
        self._session_id = data.get('session_id') #cache session_id for use in resume
        self.private_channels = data.get('private_channels')
        for guild in data.get('guilds'):
            self.guilds[guild.get('id')] = {'unavailable':True}

    #processes guild create event to fill in self.guilds
    async def _processGuildCreate(self, data):
        #find a guild with a matching id
        #https://stackoverflow.com/questions/8653516/python-list-of-dictionaries-search
        self.guilds[data.get('id')] = data
        # self.guilds[data['id']].pop('id', None) #remove id from the dict, it's already our key


    async def _reconnect(self, code = 1000, reason = '', resume = False):
        await self.stop(code=code, reason = reason)
        self._event_loop.create_task(self.start(resume)) #restart handshake process if no heartbeat acknowledge

    ##
    # Starts the client.
    # \param[in] resume If true, will attempt to resume a session. Default is ``false``.
    ##
    async def start(self, resume=False):
        self._log.info("{client_name} ({app_name}) is starting...".format(client_name=__name__, app_name=self._name))

        gateway_args = {'v':self.api_version, 'encoding':self.encoding} #recommended to explicitly define which gateway and encoding version
        disc_hello = await self._connect(self._cached_gateway, gateway_args)
        self._event_loop.create_task(self._processPayload(disc_hello))
        if (resume): #if told to resume, resume
            self._event_loop.create_task(self._resume())
        else:
            self._event_loop.create_task(self._identify())
        self._event_loop.create_task(self._receivePayloads())


    ##
    # Stops the client.
    # \param[in] code Code to return to endpoint on close.
    # \param[in] reason The text reason for closing the connection.
    ##
    async def stop(self, code = 1000, reason = ''):
        await self._websocket.close(code=code, reason = reason)
        self._log.info("{client_name} ({app_name}) has shutdown.".format(client_name=__name__, app_name=self._name))

    ##
    # Check if there is a payload available to be processed.
    #
    # Payloads available are all non-connection specific payloads.
    # \return ``True`` if not empty, ``False`` if empty.
    ##
    def hasPayload(self):
        return (not self._payloads.empty())
    ##
    # Returns the oldest payload available.
    #
    # \return Payload object, of type similar to GatewayPayloadObject.
    #
    ##
    def getPayload(self):
        return self._payloads.get()

    #END: Gateway API Functions
