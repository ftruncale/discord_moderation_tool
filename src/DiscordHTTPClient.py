#Francesca 'Jade' Truncale, 2020
## @package DiscordHTTPClient
#  Wrapper for Discord's HTTP API, located here: #https://discordapp.com/developers/docs/
#
##
import asyncio
import aiohttp #asyncronous http calls

##
#  Wrapper for Discord's HTTP API, documentation here:
#
#  https://discordapp.com/developers/docs/
#
##
class DiscordHTTPClient():
    ##
    # \param token User or Bot token, obtained from [Discord's Developer Portal](https://discord.com/developers/applications)
    def __init__(self, token):
        self._base_url = 'https://discordapp.com/api'
        self._api_version =  '6';
        self._encoding = 'json'

        self._token = token

        self._headers = {'User-Agent': "DiscordBot ({base_url}, {api_version})".format(base_url=self._base_url, api_version = self._api_version),
                        'Authorization': 'Bot '+ self._token} #for use with http api calls

        #https://stackoverflow.com/questions/53496781/aiohttp-how-to-save-a-persistent-clientsession-in-a-class
        #Referenced the above
        self._session = aiohttp.ClientSession(headers=self._headers)

    ##
    # Wrapper for Get Channel Messages
    # \param channel_id The channel id to get messages from.
    # \param limit The number of messages to receive in one call. Default is ``50``.
    # \param message_id Start getting messages from this message_id.
    # \param when Get messages from ``"around"``, ``"before"``, ``"after"`` the supplied message_id.
    # \returns JSON Array of matching messages.
    ##
    async def getMessages(self, channel_id, limit = 50, message_id = '', when = ''): #Gets channel messages, including query params. correct values for when are before, after, and around strings
        params = {'limit': limit, when : message_id}
        async with self._session.get(self._base_url+"/channels/{channel_id}/messages".format(channel_id=channel_id),
                                     params=params) as resp:
            return await resp.json()

    ##
    # Wrapper for Get Channel Message
    # \param channel_id The channel id to get message from.
    # \param message_id The exact message to get.
    # \returns JSON dict of matching message.
    ##
    async def getMessage(self, channel_id, message_id): #Gets channel messages, including query params. correct values for when are before, after, and around strings
        async with self._session.get(self._base_url+"/channels/{channel_id}/messages/{message_id}".format(channel_id=channel_id,
                                                                                                          message_id=message_id)) as resp:
            return await resp.json()

    ##
    # Wrapper for Create Guild Ban
    # \param guild_id The guild to ban the user from.
    # \param user_id The user to ban.
    # \param reason A ``String`` reason for banning.
    # \param delete_message_days Remove messages from the user from the specified number of days.
    # \param when Get messages from ``"around"``, ``"before"``, ``"after"`` the supplied message_id.
    # \returns JSON Array of matching messages.
    ##
    async def guildBan(self, guild_id, user_id, delete_message_days, reason):
        params = {'delete-message-days':delete_message_days, 'reason':reason}
        async with self._session.put(self._base_url+"/guilds/{guild_id}/bans/{user_id}".format(guild_id=guild_id, user_id=user_id),
                                     params = params) as resp:
            return await resp.json()
    ##
    # Wrapper for Get User
    # \param user_id The user to get.
    # \returns JSON dict of matching user.
    ##
    async def getUser(self, user_id):
        async with self._session.get(self._base_url+"/users/{user_id}".format(user_id=user_id)) as resp:
            return await resp.json()
