#Francesca 'Jade' Truncale, 2020
## @package models
# \brief Helper module to streamline sqlalchemy usage.
##
from sqlalchemy import Column, Integer, String, Unicode, Boolean, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from marshmallow import Schema, fields #to prepare to serialize models to json

Base = declarative_base()
#represents the Message table in the database
class Message(Base):
    __tablename__ = 'message'

    id = Column(String, primary_key = True)
    user_id = Column(String)
    channel_id = Column(String, ForeignKey('channel.id'))
    timestamp = Column(String)
    edited_timestamp = Column(String)
    has_attachment = Column(Boolean)
    content = Column(Unicode)
    has_embed = Column(Boolean)
    deleted = Column(Boolean)

#represents the channel table
class Channel(Base):
    __tablename__ = 'channel'

    id = Column(Integer, primary_key = True)
    guild_id = Column(Integer)

#represents the attachment table
class Attachment(Base):
    __tablename__ = 'attachment'

    id = Column(String, primary_key = True)
    message_id = Column(String, ForeignKey('message.id'))
    filename = Column(Unicode)
    url = Column(Unicode)

#represents the embed table
class Embed(Base):
    __tablename__ = 'embed'

    embed_num = Column(Integer, primary_key = True, autoincrement = True)
    message_id = Column(String, ForeignKey('message.id'))
    type = Column(String)
    title = Column(Unicode)
    description = Column(Unicode)
    url = Column(Unicode)
    timestamp  = Column(String)
    color = Column(String)



class Edited_Message(Base): #purposefully using non camel case to match sql table name
    __tablename__ = 'edited_message'

    id = Column(Integer, primary_key = True, autoincrement = True)
    message_id = Column(String, ForeignKey('message.id'))
    edited_timestamp = Column(String)
    content = Column(Unicode)


class MessageSchema(Schema):
    id = fields.String()
    user_id = fields.String()
    channel_id = fields.String()
    timestamp = fields.String()
    edited_timestamp = fields.String()
    has_attachment = fields.Boolean()
    content = fields.String()
    has_embed = fields.Boolean()
    deleted = fields.Boolean()

class ChannelSchema(Schema):
    id = fields.String()
    guild_id = fields.String()
