#Francesca 'Jade' Truncale, 2020
## \mainpage Introduction (Version 1.0)
# \image html screenshot.png width=512px
#
# Welcome to the Documentation for the Discord Moderation Tool.
#
# The DMT is a moderation tool for Discord servers.
# It currently features a chat logging function, and a frontend to view these chat logs.
#
# This project is maintained on [Bitbucket](https://bitbucket.org/ftruncale/discord_moderation_tool/).
#
# Released under GPLv3.
#
# Copyright 2020 [Jade Truncale](https://ftruncale.bitbucket.io/)
##

import DiscordModerationTool as DMT

def main():
    client = DMT.DiscordModerationTool(config='DMT.ini')
if __name__ == '__main__':
    main()
