var searchData=
[
  ['gatewaypayloadobject_15',['GatewayPayloadObject',['../classDiscordGatewayClient_1_1GatewayPayloadObject.html',1,'DiscordGatewayClient']]],
  ['getmessage_16',['getMessage',['../classDiscordHTTPClient_1_1DiscordHTTPClient.html#a9357153aa03e2ffdc7a6b722b0c6d4ea',1,'DiscordHTTPClient::DiscordHTTPClient']]],
  ['getmessages_17',['getMessages',['../classDiscordHTTPClient_1_1DiscordHTTPClient.html#a683d092e63f0fda239a4d50ebe5919f5',1,'DiscordHTTPClient::DiscordHTTPClient']]],
  ['getpayload_18',['getPayload',['../classDiscordGatewayClient_1_1DiscordGateWayClient.html#a7d83d85f8d1aaa922ede658daa602139',1,'DiscordGatewayClient::DiscordGateWayClient']]],
  ['getuser_19',['getUser',['../classDiscordHTTPClient_1_1DiscordHTTPClient.html#a4f8cf151702457366b1b50b32b956422',1,'DiscordHTTPClient::DiscordHTTPClient']]],
  ['guildban_20',['guildBan',['../classDiscordHTTPClient_1_1DiscordHTTPClient.html#a5fac0cde9080b38e355b2693fa12bfbc',1,'DiscordHTTPClient::DiscordHTTPClient']]],
  ['guilds_21',['guilds',['../classDiscordGatewayClient_1_1DiscordGateWayClient.html#abe8ac6465f3ac8963b9e6e6aa8c080a8',1,'DiscordGatewayClient::DiscordGateWayClient']]]
];
