var indexSectionsWithContent =
{
  0: "_abcdeghimopst",
  1: "acdegm",
  2: "dm",
  3: "_ghs",
  4: "abdegopst",
  5: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Pages"
};

