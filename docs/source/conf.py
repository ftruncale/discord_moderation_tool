import sys
sys.path.append( "/home/jade/Documents/Repos/discord_moderation_tool/docs/ext/breathe/" )
extensions = ['sphinx.ext.pngmath', 'sphinx.ext.todo', 'breathe' ]
breathe_projects = { "dmt": "/home/jade/Documents/Repos/discord_moderation_tool/docs/xml" }
breathe_default_project = "dmt"
