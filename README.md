# Discord Moderation Tool
![version 1.0](https://img.shields.io/badge/Version-1.0-blue)

![Screenshot of Tool](images/screenshot.png | width=150)

The DMT is a moderation tool for Discord servers.

It currently features a chat logging function, and a frontend to view these chat logs.

Contains wrappers for the Discord Gateway and HTTP APIs.

Requires `` sqlalchemy sqlalchemy_utils aiohttp aiohttp_oauth2 aiohttp_cors marshmallow gateways requests websockets`` Python3 packages.

``requirements.txt``  available for quick install through pip.
``pip install -r requirements.txt``

Documentation is available [here](docs/html/index.html)

## Quick Start Guide
* Run ``src/main.py``
* Run the ux server in the ``frontend`` folder using ``npm run serve``

By default, the you can access the ux from ``localhost:8080``.



### End notes
Copyright 2020 [Jade Truncale](https://ftruncale.bitbucket.io/)

Released under [GPLv3](gpl-3.0.txt)
